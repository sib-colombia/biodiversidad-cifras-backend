from django.db import models


class Dominio(models.Model):
    padre = models.ForeignKey('self', null=True, related_name='dominioHijos', on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100, null=True)
    tipo = models.CharField(max_length=100, null=True)
    aplica = models.BooleanField(default=True, null=True)
    fechaModificacion = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.nombre


class GrupoBiologico(models.Model):
    padre = models.ForeignKey('self', null=True, related_name='grupoBiologicoHijos', on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100, null=True)
    tipo = models.CharField(max_length=100, null=True)
    aplica = models.BooleanField(default=True, null=True)
    fechaModificacion = models.DateTimeField(auto_now=True, null=True)
    urlImagen = models.CharField(max_length=1000, null=True)

    def __str__(self):
        return str(self.nombre)


class GrupoTaxonomico(models.Model):
    padre = models.ForeignKey('self', null=True, related_name='grupoTaxonomicoHijos', on_delete=models.CASCADE)
    padreGrupoBiologico = models.ForeignKey(GrupoBiologico, on_delete=models.CASCADE, null=True)
    gbifId = models.IntegerField(null=True)
    nombre = models.CharField(max_length=100, null=True)
    reino = models.CharField(max_length=100, null=True)
    filo = models.CharField(max_length=100, null=True)
    clase = models.CharField(max_length=100, null=True)
    orden = models.CharField(max_length=100, null=True)
    familia = models.CharField(max_length=100, null=True)
    genero = models.CharField(max_length=100, null=True)
    especie = models.CharField(max_length=100, null=True)
    aplica = models.BooleanField(default=True, null=True)
    fechaModificacion = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.nombre)


class Geografia(models.Model):
    padre = models.ForeignKey('self', null=True, related_name='geografiaHijos', on_delete=models.CASCADE)
    divipola = models.CharField(max_length=100, null=True)
    padreDivipola = models.CharField(max_length=100, null=True)
    nombre = models.CharField(max_length=100, null=True)
    tipo = models.CharField(max_length=100, null=True)
    fechaModificacion = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.nombre


class GrupoBiologicoGeografia(models.Model):
    grupoBiologico = models.ForeignKey(GrupoBiologico, related_name='grupoBiologicoGeografiaHijosBio',
                                           on_delete=models.CASCADE, null=True)
    geografia = models.ForeignKey(Geografia, related_name='grupoBiologicoGeografiaHijosGeo', on_delete=models.CASCADE, null=True)
    aplica = models.BooleanField(default=True, null=True)
    fechaModificacion = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.idDominioBiologico.nombre)


class EntidadPublicadora(models.Model):
    organizationId = models.CharField(max_length=100, null=True)
    geografiaOrigen= models.ForeignKey(Geografia, related_name='geografiaHijosEntidad', on_delete=models.CASCADE, null=True)
    organizationCountry = models.CharField(max_length=100, null=True)
    nombre = models.CharField(max_length=100, null=True)
    tipo = models.CharField(max_length=100, null=True)
    urlImagen = models.CharField(max_length=1000, null=True)
    fechaModificacion = models.DateTimeField(auto_now=True, null=True)
    def __str__(self):
        return self.nombre


class VistaGrupoBiologico(models.Model):
    grupoBiologicoGeografia = models.ForeignKey(GrupoBiologicoGeografia, related_name='vistasGrupos',
                                                    on_delete=models.CASCADE, null=True)
    especiesEstimadasPais = models.IntegerField(null=True)
    especies = models.IntegerField(null=True)
    registros = models.IntegerField(null=True)
    especiesAmenaza = models.IntegerField(null=True)
    especiesVU = models.IntegerField(null=True)
    especiesEN = models.IntegerField(null=True)
    especiesCR = models.IntegerField(null=True)
    registrosAmenaza = models.IntegerField(null=True)
    registrosVU = models.IntegerField(null=True)
    registrosEN = models.IntegerField(null=True)
    registrosCR = models.IntegerField(null=True)
    especiesCites = models.IntegerField(null=True)
    especiesCitesI = models.IntegerField(null=True)
    especiesCitesII = models.IntegerField(null=True)
    especiesCitesIII = models.IntegerField(null=True)
    registrosCites = models.IntegerField(null=True)
    registrosCitesI = models.IntegerField(null=True)
    registrosCitesII = models.IntegerField(null=True)
    registrosCitesIII = models.IntegerField(null=True)
    especiesEndemicas = models.IntegerField(null=True)
    especiesExoticas = models.IntegerField(null=True)
    especiesMigratorias = models.IntegerField(null=True)
    registrosEndemicas = models.IntegerField(null=True)
    registrosExoticas = models.IntegerField(null=True)
    registrosMigratorias = models.IntegerField(null=True)
    tipoReporte = models.CharField(max_length=100)
    fechaModificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.dominioBiologicoGeografia.dominioBiologico.nombre


class VistaGrupoBiologicoHistorico(models.Model):
    grupoBiologicoGeografia = models.ForeignKey(GrupoBiologicoGeografia, related_name='vistasGruposHistorica',
                                                  on_delete=models.CASCADE,null=True)
    especiesEstimadasPais = models.IntegerField(null=True)
    registros = models.IntegerField(null=True)
    especies = models.IntegerField(null=True)
    registrosAmenaza = models.IntegerField(null=True)
    especiesAmenaza = models.IntegerField(null=True)
    registrosVU = models.IntegerField(null=True)
    especiesVU = models.IntegerField(null=True)
    registrosEN = models.IntegerField(null=True)
    especiesEN = models.IntegerField(null=True)
    registrosCR = models.IntegerField(null=True)
    especiesCR = models.IntegerField(null=True)
    registrosCitesI = models.IntegerField(null=True)
    registrosCitesI = models.IntegerField(null=True)
    registrosCitesII = models.IntegerField(null=True)
    especiesCitesII = models.IntegerField(null=True)
    registrosCitesIII = models.IntegerField(null=True)
    especiesCitesIII = models.IntegerField(null=True)
    registrosEndemicas = models.IntegerField(null=True)
    especiesEndemicas = models.IntegerField(null=True)
    registrosMigratorias = models.IntegerField(null=True)
    especiesMigratorias = models.IntegerField(null=True)
    registrosExoticas = models.IntegerField(null=True)
    especiesExoticas = models.IntegerField(null=True)
    tipoReporte = models.CharField(max_length=100)
    fechaModificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.dominioBiologicoGeografia.dominioBiologico.nombre


class VistaGeografia(models.Model):
    geografia = models.ForeignKey(Geografia, related_name='vistasGeograficas',
                                                    on_delete=models.CASCADE,null=True)
    especies = models.IntegerField(null=True)
    registros = models.IntegerField(null=True)
    especiesAmenaza = models.IntegerField(null=True)
    especiesVU = models.IntegerField(null=True)
    especiesEN = models.IntegerField(null=True)
    especiesCR = models.IntegerField(null=True)
    registrosAmenaza = models.IntegerField(null=True)
    registrosVU = models.IntegerField(null=True)
    registrosEN = models.IntegerField(null=True)
    registrosCR = models.IntegerField(null=True)
    especiesCites = models.IntegerField(null=True)
    especiesCitesI = models.IntegerField(null=True)
    especiesCitesII = models.IntegerField(null=True)
    especiesCitesIII = models.IntegerField(null=True)
    registrosCites = models.IntegerField(null=True)
    registrosCitesI = models.IntegerField(null=True)
    registrosCitesII = models.IntegerField(null=True)
    registrosCitesIII = models.IntegerField(null=True)
    especiesEndemicas = models.IntegerField(null=True)
    especiesExoticas = models.IntegerField(null=True)
    especiesMigratorias = models.IntegerField(null=True)
    registrosEndemicas = models.IntegerField(null=True)
    registrosExoticas = models.IntegerField(null=True)
    registrosMigratorias = models.IntegerField(null=True)
    tipoReporte = models.CharField(max_length=100)
    fechaModificacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.idGeografia.nombre


class VistaGeografiaHistorico(models.Model):
    geografia = models.ForeignKey(Geografia, related_name='vistasGeograficasHistoricas',
                                    on_delete=models.CASCADE,null=True)
    especies = models.IntegerField(null=True)
    registros = models.IntegerField(null=True)
    especiesAmenaza = models.IntegerField(null=True)
    especiesVU = models.IntegerField(null=True)
    especiesEN = models.IntegerField(null=True)
    especiesCR = models.IntegerField(null=True)
    registrosAmenaza = models.IntegerField(null=True)
    registrosVU = models.IntegerField(null=True)
    registrosEN = models.IntegerField(null=True)
    registrosCR = models.IntegerField(null=True)
    especiesCites = models.IntegerField(null=True)
    especiesCitesI = models.IntegerField(null=True)
    especiesCitesII = models.IntegerField(null=True)
    especiesCitesIII = models.IntegerField(null=True)
    registrosCites = models.IntegerField(null=True)
    registrosCitesI = models.IntegerField(null=True)
    registrosCitesII = models.IntegerField(null=True)
    registrosCitesIII = models.IntegerField(null=True)
    especiesEndemicas = models.IntegerField(null=True)
    especiesExoticas = models.IntegerField(null=True)
    especiesMigratorias = models.IntegerField(null=True)
    registrosEndemicas = models.IntegerField(null=True)
    registrosExoticas = models.IntegerField(null=True)
    registrosMigratorias = models.IntegerField(null=True)
    tipoReporte = models.CharField(max_length=100,null=True)
    fechaModificacion = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.idGeografia.id


class EntidadPublicadoraGeografia(models.Model):
    entidadPublicadora = models.ForeignKey(EntidadPublicadora, related_name='entidadPublicadoraGeografiaHijosEnt',
                                  on_delete=models.CASCADE,null=True)
    geografia = models.ForeignKey(Geografia, related_name='entidadPublicadoraGeografiaHijosGeo', on_delete=models.CASCADE,null=True)
    aplica = models.BooleanField(default=True,null=True)
    fechaModificacion = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.idEntidad.nombre

class VistaEntidadPublicadora(models.Model):
    entidadPublicadoraGeografia = models.ForeignKey(EntidadPublicadoraGeografia, on_delete=models.CASCADE,null=True)
    registros = models.IntegerField(null=True)
    especies = models.IntegerField(null=True)
    fechaModificacion = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.idEntidad.nombre

class VistaEntidadPublicadoraHistorico(models.Model):
    entidadPublicadoraGeografia = models.ForeignKey(EntidadPublicadoraGeografia, related_name='vistaEntidadPublicadora', on_delete=models.CASCADE,null=True)
    registros = models.IntegerField(null=True)
    especies = models.IntegerField(null=True)
    fechaModificacion = models.DateTimeField(auto_now=True,null=True)

    def __str__(self):
        return self.idEntidad.nombre