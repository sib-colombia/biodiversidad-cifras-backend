import graphene
from graphene_django.types import DjangoObjectType
from Api.models import Dominio, Geografia, GrupoBiologico, GrupoBiologicoGeografia, GrupoTaxonomico, \
    VistaGrupoBiologico, VistaGeografia, EntidadPublicadora, VistaEntidadPublicadora, VistaGrupoBiologicoHistorico, \
    VistaGeografiaHistorico, VistaEntidadPublicadoraHistorico, EntidadPublicadoraGeografia
from Api.Util import Constants
from django.core.cache import cache
import logging
import traceback

logger = logging.getLogger('logger-biodiversidad-cifras')


class DominioType(DjangoObjectType):
    class Meta:
        model = Dominio


class GeografiaType(DjangoObjectType):
    class Meta:
        model = Geografia


class VistaGeograficaType(DjangoObjectType):
    class Meta:
        model = VistaGeografia


class VistaGeografiaHistoricoType(DjangoObjectType):
    class Meta:
        model = VistaGeografiaHistorico


class GrupoTaxonomicoType(DjangoObjectType):
    class Meta:
        model = GrupoTaxonomico


class GrupoBiologicoType(DjangoObjectType):
    class Meta:
        model = GrupoBiologico


class GrupoBiologicoGeografiaType(DjangoObjectType):
    class Meta:
        model = GrupoBiologicoGeografia


class VistaGrupoBiologicoType(DjangoObjectType):
    class Meta:
        model = VistaGrupoBiologico


class VistaGrupoBiologicoHistoricoType(DjangoObjectType):
    class Meta:
        model = VistaGrupoBiologicoHistorico


class EntidadPublicadoraType(DjangoObjectType):
    class Meta:
        model = EntidadPublicadora


class EntidadPublicadoraGeografiaType(DjangoObjectType):
    class Meta:
        model = EntidadPublicadoraGeografia


class VistaEntidadPublicadoraType(DjangoObjectType):
    class Meta:
        model = VistaEntidadPublicadora


class VistaEntidadPublicadoraHistoricoType(DjangoObjectType):
    class Meta:
        model = VistaEntidadPublicadoraHistorico


class Query(object):

    def _init_(self):
        print('Schema')

    def obtainResultQuery(self, **kwargs):
        result = None
        try:
            query = kwargs.get('query')
            queryKey = kwargs.get('queryKey')
            if (cache.get(queryKey) is None):
                #TODO: VALIDAR TAMAÑO DEL RESULTADO
                result = query
                cache.set(queryKey, result, Constants.CACHE_TIMEOUT)
                logger.info(Constants.MENSAJE_CONSULTA_DB + queryKey)
            else:
                result = cache.get(queryKey)
                logger.info(Constants.MENSAJE_CONSULTA_CACHE + queryKey)
        except Exception as e:
            logger.error(Constants.MENSAJE_EXCEPCION_CONSULTA + queryKey)
            logger.exception(traceback.print_exc())
        return result

    grupoBiologico = graphene.Field(GrupoBiologicoType, id=graphene.Int())

    all_grupoBiologico = graphene.List(GrupoBiologicoType)

    def resolve_grupoBiologico(self, info, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            queryKey = Constants.QUERY_GRUPO_BIOLOGICO + Constants.QUERY_KEY_SEPARATOR + str(id)
            return Query.obtainResultQuery(self, query=GrupoBiologico.objects.prefetch_related(
                'grupoBiologicoHijos', 'grupoBiologicoHijos__grupoBiologicoHijos',
                'grupoBiologicoHijos__grupoBiologicoHijos__grupoBiologicoHijos').get(pk=id), queryKey=queryKey)

    def resolve_all_grupoBiologico(self, info, **kwargs):
        return Query.obtainResultQuery(self, query=GrupoBiologico.objects.all().prefetch_related('grupoBiologicoHijos',
                                                                                                 'grupoBiologicoHijos__grupoBiologicoHijos',
                                                                                                 'grupoBiologicoHijos__grupoBiologicoHijos__grupoBiologicoHijos'),
                                       queryKey=Constants.QUERY_ALL_GRUPO_BIOLOGICO)

    # Queries vista grupo biologico

    all_vistaGrupoBiologico = graphene.List(VistaGrupoBiologicoType)

    vista_grupo_biologico_by_geografia = graphene.List(VistaGrupoBiologicoType, geografiaId=graphene.Int())

    vista_grupo_biologico_by_geografia_padre = graphene.List(VistaGrupoBiologicoType, geografiaPadreId=graphene.Int())

    def resolve_all_vistaGrupoBiologico(self, info, **kwargs):
        result = Query.obtainResultQuery(self, query=VistaGrupoBiologico.objects.all().select_related(
            'grupoBiologicoGeografia', 'grupoBiologicoGeografia__geografia', 'grupoBiologicoGeografia__grupoBiologico'),
                                         queryKey=Constants.QUERY_ALL_VISTA_GRUPO_BIOLOGICO)
        return result

    def resolve_vista_grupo_biologico_by_geografia(self, info, **kwargs):
        geografiaId = kwargs.get('geografiaId')
        if geografiaId is not None:
            queryKey = Constants.QUERY_VISTA_GRUPO_BY_GEOGRAFIA + Constants.QUERY_KEY_SEPARATOR + str(geografiaId)
            return Query.obtainResultQuery(self, query=VistaGrupoBiologico.objects.filter(
                grupoBiologicoGeografia__geografia__id=geografiaId).select_related('grupoBiologicoGeografia',
                                                                                   'grupoBiologicoGeografia__geografia',
                                                                                   'grupoBiologicoGeografia__grupoBiologico'),
                                           queryKey=queryKey)

    def resolve_vista_grupo_biologico_by_geografia_padre(self, info, **kwargs):
        geografiaPadreId = kwargs.get('geografiaPadreId')
        if geografiaPadreId is not None:
            queryKey = Constants.QUERY_VISTA_GRUPO_BY_GEOGRAFIA_PADRE + Constants.QUERY_KEY_SEPARATOR + str(
                geografiaPadreId)
            return Query.obtainResultQuery(self, query=VistaGrupoBiologico.objects.filter(
                grupoBiologicoGeografia__geografia__padre__id=geografiaPadreId).prefetch_related(
                'grupoBiologicoGeografia', 'grupoBiologicoGeografia__geografia'),
                                           queryKey=queryKey)

    # Queries de relacion grupo biologico y geografia

    grupoBiologicoGeografia = graphene.Field(GrupoBiologicoGeografiaType, id=graphene.Int())

    all_grupoBiologicoGeografia = graphene.List(GrupoBiologicoGeografiaType)

    grupo_geografia_by_geografia = graphene.List(GrupoBiologicoGeografiaType, geografiaId=graphene.Int())

    def resolve_grupoBiologicoGeografia(self, info, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            queryKey = Constants.QUERY_GRUPO_GEOGRAFIA + Constants.QUERY_KEY_SEPARATOR + str(id)
            return Query.obtainResultQuery(self, query=GrupoBiologico.objects.prefetch_related(
                'grupoBiologicoHijos', 'grupoBiologicoHijos__grupoBiologicoHijos',
                'grupoBiologicoHijos__grupoBiologicoHijos__grupoBiologicoHijos').get(pk=id), queryKey=queryKey)

    def resolve_all_grupoBiologicoGeografia(self, info, **kwargs):
        return Query.obtainResultQuery(
            query=GrupoBiologicoGeografia.objects.all().select_related('geografia', 'grupoBiologico'),
            queryKey=Constants.QUERY_ALL_GRUPO_GEOGRAFIA)

    def resolve_grupo_geografia_by_geografia(self, info, **kwargs):
        geografiaId = kwargs.get('geografiaId')
        if geografiaId is not None:
            queryKey = Constants.QUERY_GRUPO_GEOGRAFIA_BY_GEOGRAFIA + Constants.QUERY_KEY_SEPARATOR + str(geografiaId)
            return Query.obtainResultQuery(self, query=GrupoBiologicoGeografia.objects.filter(
                geografia__id=geografiaId).prefetch_related('geografia',
                                                            'grupoBiologico__grupoBiologicoHijos__grupoBiologicoHijos__grupoBiologicoHijos',
                                                            'vistasGrupos'), queryKey=queryKey)

    # Queries de geografia

    geografia = graphene.Field(GeografiaType, id=graphene.Int())

    all_geografia = graphene.List(GeografiaType)

    def resolve_all_geografia(self, info, **kwargs):
        return Query.obtainResultQuery(self, query=Geografia.objects.all().prefetch_related('geografiaHijos',
                                                                                            'geografiaHijos__geografiaHijos'),
                                       queryKey=Constants.QUERY_ALL_GEOGRAFIA)

    def resolve_geografia(self, info, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            queryKey = Constants.QUERY_GEOGRAFIA + Constants.QUERY_KEY_SEPARATOR + str(id)
            return Query.obtainResultQuery(self, query=Geografia.objects.prefetch_related('geografiaHijos',
                                                                                          'geografiaHijos__geografiaHijos').get(
                pk=id), queryKey=queryKey)

    # Queries de vista de geografia

    vistaGeografia = graphene.Field(VistaGeograficaType, id=graphene.Int())

    all_vistaGeografia = graphene.List(VistaGeograficaType)

    vista_geo_by_geografia_padre = graphene.List(VistaGeograficaType, geografiaPadreId=graphene.Int())

    vista_geo_by_geografia = graphene.List(VistaGeograficaType, geografiaId=graphene.Int())

    def resolve_vistaGeografia(self, info, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            queryKey = Constants.QUERY_VISTA_GEOGRAFICA + Constants.QUERY_KEY_SEPARATOR + str(id)
            return Query.obtainResultQuery(self, query=VistaGeografia.objects.get(pk=id), queryKey=queryKey)

    def resolve_all_vistaGeografia(self, info, **kwargs):
        return Query.obtainResultQuery(self, query=VistaGeografia.objects.all().prefetch_related('geografia'),
                                       queryKey=Constants.QUERY_ALL_VISTA_GEOGRAFICA)

    def resolve_vista_geo_by_geografia_padre(self, info, **kwargs):
        geografiaPadreId = kwargs.get('geografiaPadreId')
        if geografiaPadreId is not None:
            queryKey = Constants.QUERY_VISTA_GEO_BY_GEOGRAFIA_PADRE + Constants.QUERY_KEY_SEPARATOR + str(
                geografiaPadreId)
            return Query.obtainResultQuery(self, query=VistaGeografia.objects.select_related('geografia').filter(
                geografia__padre__id=geografiaPadreId), queryKey=queryKey)

    def resolve_vista_geo_by_geografia(self, info, **kwargs):
        geografiaId = kwargs.get('geografiaId')
        if geografiaId is not None:
            queryKey = Constants.QUERY_VISTA_GEO_BY_GEOGRAFIA + Constants.QUERY_KEY_SEPARATOR + str(geografiaId)
            return Query.obtainResultQuery(self, query=VistaGeografia.objects.filter(
                geografia__id=geografiaId).prefetch_related('geografia'), queryKey=queryKey)

    # Queries de entidad publicadora

    entidadPublicadora = graphene.Field(EntidadPublicadoraType, id=graphene.Int())

    all_entidadPublicadora = graphene.List(EntidadPublicadoraType)

    entidadPublicadora_by_geografiaOrigen = graphene.List(EntidadPublicadoraType, geografiaOrigenId=graphene.Int())

    def resolve_all_entidadPublicadora(self, info, **kwargs):
        return Query.obtainResultQuery(self, query=EntidadPublicadora.objects.all().prefetch_related(
            'entidadPublicadoraGeografiaHijosEnt'), queryKey=Constants.QUERY_ALL_ENTIDAD_PUBLICADORA)

    def resolve_entidadPublicadora(self, info, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            queryKey = Constants.QUERY_ENTIDAD_PUBLICADORA + Constants.QUERY_KEY_SEPARATOR + str(id)
            return Query.obtainResultQuery(self, query=EntidadPublicadora.objects.get(pk=id), queryKey=queryKey)

    def resolve_entidadPublicadora_by_geografiaOrigen(self, info, **kwargs):
        geografiaOrigenId = kwargs.get('geografiaOrigenId')
        if geografiaOrigenId is not None:
            queryKey = Constants.QUERY_ENTIDAD_PUBLICADORA_BY_GEOGRAFIA + Constants.QUERY_KEY_SEPARATOR + str(
                geografiaOrigenId)
            return Query.obtainResultQuery(self, query=EntidadPublicadora.objects.filter(
                geografiaOrigen__id=geografiaOrigenId).prefetch_related(
                'geografiaOrigen'), queryKey=queryKey)

    # Queries de vista de entidad publicadora

    vista_entidad_publicadora = graphene.List(VistaEntidadPublicadoraType, id=graphene.Int())

    all_vista_entidad_publicadora = graphene.List(VistaEntidadPublicadoraType)

    vista_entidad_publicadora_by_geografia = graphene.List(VistaEntidadPublicadoraType, geografiaId=graphene.Int())

    def resolve_vista_entidad_publicadora(self, info, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            queryKey = Constants.QUERY_VISTA_ENTIDAD_PUBLICADORA + Constants.QUERY_KEY_SEPARATOR + str(id)
            return Query.obtainResultQuery(self, query=VistaEntidadPublicadora.objects.select_related(
                'entidadPublicadoraGeografia',
                'entidadPublicadoraGeografia__geografia',
                'entidadPublicadoraGeografia__entidadPublicadora').get(
                pk=id), queryKey=queryKey)

    def resolve_all_vista_entidad_publicadora(self, info, **kwargs):
        return Query.obtainResultQuery(self, query=VistaEntidadPublicadora.objects.all().prefetch_related(
            'entidadPublicadoraGeografia',
            'entidadPublicadoraGeografia__geografia',
            'entidadPublicadoraGeografia__entidadPublicadora'), queryKey=Constants.QUERY_ALL_VISTA_ENTIDAD_PUBLICADORA)

    def resolve_vista_entidad_publicadora_by_geografia(self, info, **kwargs):
        geografiaId = kwargs.get('geografiaId')
        if geografiaId is not None:
            queryKey = Constants.QUERY_VISTA_ENTIDAD_PUBLICADORA_BY_GEO + Constants.QUERY_KEY_SEPARATOR + str(geografiaId)
            return Query.obtainResultQuery(self, query=VistaEntidadPublicadora.objects.filter(
                entidadPublicadoraGeografia__geografia__id=geografiaId).select_related(
                'entidadPublicadoraGeografia__geografia', 'entidadPublicadoraGeografia__entidadPublicadora'), queryKey=queryKey)


    # Queries de relacion entidad publicadore y geografia

    entidad_publicadora_geografia = graphene.Field(EntidadPublicadoraGeografiaType, id=graphene.Int())

    all_entidad_publicadora_geografia = graphene.List(EntidadPublicadoraGeografiaType)

    entidad_publicadora_geografia_by_geografia = graphene.List(EntidadPublicadoraGeografiaType,
                                                               geografiaId=graphene.Int())

    def resolve_entidad_publicadora_geografia(self, info, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            queryKey = Constants.QUERY_ENTIDAD_GEOGRAFIA + Constants.QUERY_KEY_SEPARATOR + str(id)
            return Query.obtainResultQuery(self, query=EntidadPublicadoraGeografia.objects.select_related('geografia', 'entidadPublicadora').get(pk=id),queryKey=queryKey)




    def resolve_all_entidad_publicadora_geografia(self, info, **kwargs):
        return Query.obtainResultQuery(self, query=EntidadPublicadoraGeografia.objects.all().select_related('geografia',
                                                                                                            'entidadPublicadora'),
                                       queryKey=Constants.QUERY_ALL_ENTIDAD_GEOGRAFIA)

    def resolve_entidad_publicadora_geografia_by_geografia(self, info, **kwargs):
        geografiaId = kwargs.get('geografiaId')
        if geografiaId is not None:
            queryKey = Constants.QUERY_ENTIDAD_GEOGRAFIA_BY_GEO + Constants.QUERY_KEY_SEPARATOR + str(geografiaId)
            return Query.obtainResultQuery(self, query=EntidadPublicadoraGeografia.objects.select_related('geografia', 'entidadPublicadora').get(pk=geografiaId),queryKey=queryKey)