#Query Identification
QUERY_KEY_SEPARATOR='-'
QUERY_GRUPO_BIOLOGICO='grupoBiologico'
QUERY_ALL_GRUPO_BIOLOGICO='allGrupoBiologico'
QUERY_ALL_VISTA_GRUPO_BIOLOGICO='allVistaGrupoBiologico'
QUERY_VISTA_GRUPO_BY_GEOGRAFIA=' vistaGrupoBiologicoByGeografia'
QUERY_VISTA_GRUPO_BY_GEOGRAFIA_PADRE=' vistaGrupoBiologicoByGeografiaPadre'
QUERY_GRUPO_GEOGRAFIA= 'grupoBiologicoGeografia'
QUERY_ALL_GRUPO_GEOGRAFIA= 'allGrupoBiologicoGeografia'
QUERY_GRUPO_GEOGRAFIA_BY_GEOGRAFIA= 'grupoGeografiaByGeografia'
QUERY_ALL_GEOGRAFIA='allGeografia'
QUERY_GEOGRAFIA='geografia'
QUERY_ALL_VISTA_GEOGRAFICA='allVistaGeografica'
QUERY_VISTA_GEOGRAFICA='vistaGeografica'
QUERY_VISTA_GEO_BY_GEOGRAFIA_PADRE='vistaGeoByGeografiaPadre'
QUERY_VISTA_GEO_BY_GEOGRAFIA='vistaGeoByGeografia'
QUERY_ALL_ENTIDAD_PUBLICADORA='allEntidadPublicadora'
QUERY_ENTIDAD_PUBLICADORA='entidadPublicadora'
QUERY_ENTIDAD_PUBLICADORA_BY_GEOGRAFIA='entidadPublicadoraByGeografia'
QUERY_ALL_VISTA_ENTIDAD_PUBLICADORA='allVistaEntidadPublicadora'
QUERY_VISTA_ENTIDAD_PUBLICADORA='vistaEntidadPublicadora'
QUERY_VISTA_ENTIDAD_PUBLICADORA_BY_GEO='vistaEntidadPublicadoraByGeografia'
QUERY_ALL_ENTIDAD_GEOGRAFIA='allEntidadPublicadoraGeografia'
QUERY_ENTIDAD_GEOGRAFIA='entidadPublicadoraGeografia'
QUERY_ENTIDAD_GEOGRAFIA='entidadPublicadoraGeografia'
QUERY_ENTIDAD_GEOGRAFIA_BY_GEO='entidadPublicadoraGeografiaByGeo'


#Deploy Properties
CACHE_TIMEOUT=36000

#Message Properties
MENSAJE_EXCEPCION_CONSULTA='Error en la consulta a base de datos. Verifique la conexión a la base de datos. La query que se intento ejecutar fue: '
MENSAJE_CONSULTA_DB="Se realiza consulta a la base de datos. La query ejecutada fue: "
MENSAJE_CONSULTA_CACHE="Se realiza la consulta sobre cache de la aplicación. El resultado corresponde a la query: "