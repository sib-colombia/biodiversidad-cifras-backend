# BioDiversidadCifrasApiBackend

* Se requiere instalar python3.7. Para hacer la instalación dirijase al repositorio de documentación de TI del EC-SIB: https://gitlab.com/sib-colombia/documentacion-productos-y-servicios

* Instalar pip para python 3
sudo apt install python3-pip

# Instalar virtualenv:
python3.7 -m pip install --user virtualenv
python3.5 -m pip install --user virtualenv

# Crear carpeta de ambientes virtuales de python
sudo mkdir -p /opt/virtual-envs-python



# Crear ambiente virtual python3.7
python3.7 -m virtualenv env-python3.7 --python=python3.7

# Crear ambiente virtual python3.5
python3.5 -m virtualenv env-python3.5 --python=python3.5

# Activar ambiente virtual 
source /opt/virtual-envs-python/env-python3.7/bin/activate
source /opt/virtual-envs-python/env-python3.5/bin/activate

# Desactivar ambiente virtual
deactivate

# Instalar dependencias mediante pip con el archivo:
pip install -r requirements.txt

# Correr las migraciones del modelo de datos del API.
python manage.py makemigrations Api
python manage.py migrate

# Iniciar el servidor con Gunicorn con 6 workers. Se debe iniciar en modo demonio con el comando nohup.
nohup gunicorn --bind 0.0.0.0:8001 --workers 6 BiodiversidadCifras.wsgi &
