## 1. API Ventanas Regionales.

El componente backend de la ventanas regionales esta desarrollado sobre el framework Django de python. El servicio del backend se expone mediante la tecnoloía Graphql, la cúal fue desarrollado por facebook y se perfila como el futuro sustituto del estílo de arquitectura REST.

Graphql trae por defecto una interfaz web en la cúal se pueden realizar consultas y se implementa la dcumentación del API con base al código implementado. Dicha interfaz consiste en estos tres componentes: 

![](https://docutopia.tupale.co/uploads/upload_9c01d7fcda1e81f39865b894fab2df9a.png)

<br/>

**1.** Query de consulta: La estructura de la query se describe en el siguiente ejemplo: 

```bash=
{
  grupoBiologico(id: 3){
    aplica
    nombre
    padre {
      id
    }
    grupoBiologicoHijos {
      id
    }
  }
}
```
Explicación: En este ejemplo se quiere consultar el grupo biológico con id 3 y además se quieren trater los campo: aplica, nombre, campo **"id"** del objeto relacionado **"padre"** y los campos **"id"** de los grupos biológicos hijos.

**2.** Es el resultado de la consulta retornado por el servidor. La estructura en la que siempre retornará el resultados será: 



```bash=
{
  "data": {
    "grupoBiologico": {
      "aplica": true,
      "nombre": "MAMÍFEROS",
      "padre": {
        "id": "2"
      },
      "grupoBiologicoHijos": [
        {
         "id": "4"
        }
      ]
    }
  }  
}  
```

**3.** Es la documentación del tipo de datos y estructura. Esta documentación se deduce de la implementación del código.

Las queries que se pueden consultar son las siguientes: 

* grupoBiologico
* allGrupoBiologico
* allVistaGrupoBiologico
* vistaGrupoBiologicoByGeografia
* vistaGrupoBiologicoByGeografiaPadre
* grupoBiologicoGeografia
* allGrupoBiologicoGeografia
* grupoGeografiaByGeografia
* allGeografia
* geografia
* allVistaGeografica
* vistaGeografica
* vistaGeoByGeografiaPadre
* vistaGeoByGeografia
* allEntidadPublicadora
* entidadPublicadora
* entidadPublicadoraByGeografia
* allVistaEntidadPublicadora
* vistaEntidadPublicadora
* vistaEntidadPublicadoraByGeografia
* allEntidadPublicadoraGeografia
* entidadPublicadoraGeografia
* entidadPublicadoraGeografia
* entidadPublicadoraGeografiaByGeo


La interfaz gráfica tiene facilidad de autocompletado de la consulta, así como sugerencias. Si quiere obtener un mayor detalle sobre la estructura de las queries en graphql diríjase a la siguiente página: https://graphql.org/learn/queries/

<br/>
